package com.example.seanwiseman.twodbpowermanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Sean on 13/05/2016.
 */
public class RebootAlarmReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context arg0, Intent arg1) {
        RebootUtil rebootUtil = new RebootUtil();
        rebootUtil.reboot();
    }
}
