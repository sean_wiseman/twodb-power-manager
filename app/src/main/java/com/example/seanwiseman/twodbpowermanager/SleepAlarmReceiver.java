package com.example.seanwiseman.twodbpowermanager;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Sean on 13/05/2016.
 */
public class SleepAlarmReceiver extends BroadcastReceiver {

    String mLogTag = this.getClass().getSimpleName();

    @Override
    public void onReceive(Context arg0, Intent arg1) {
        Context mMainContext = arg0.getApplicationContext();
        ComponentName cn = new ComponentName(arg0, AdminReceiver.class);
        DevicePolicyManager mgr = (DevicePolicyManager) arg0.getSystemService(arg0.DEVICE_POLICY_SERVICE);
        LockScreenUtil mLockScreenMgr = new LockScreenUtil(mMainContext, mgr, cn);
        mLockScreenMgr.lockMeNow();
    }
}
