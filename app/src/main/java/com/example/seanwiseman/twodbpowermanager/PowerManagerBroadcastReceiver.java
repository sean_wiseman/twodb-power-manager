package com.example.seanwiseman.twodbpowermanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;

import java.util.Calendar;

/**
 * Created by Sean on 16/05/2016.
 */
public class PowerManagerBroadcastReceiver extends BroadcastReceiver {

    String mLogTag = this.getClass().getSimpleName();
    private DevicePolicyManager mgr;
    private ComponentName cn;
    private ConfigManager mConfigMgr;

    private PendingIntent pendingSleepIntent;
    private PendingIntent pendingRebootIntent;
    private AlarmManager mAlarmManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(mLogTag, "In onReceive()");
        cn = new ComponentName(context, AdminReceiver.class);
        mgr = (DevicePolicyManager) context.getSystemService(context.DEVICE_POLICY_SERVICE);

        // Retrieve a PendingIntent that will perform a broadcast
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // setup pending intents
        Intent mSleepAlarmIntent = new Intent(context, SleepAlarmReceiver.class);
        Intent mRebootAlarmIntent = new Intent(context, RebootAlarmReceiver.class);

        pendingSleepIntent = PendingIntent.getBroadcast(context, 0, mSleepAlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        pendingRebootIntent = PendingIntent.getBroadcast(context, 0, mRebootAlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // CHECK FOR EXISTING CONFIG FILE
        mConfigMgr = new ConfigManager(context, "powerManagerConf.json");
        if (mConfigMgr.mHasBeenConfigured) {
            Log.d(mLogTag, "has been configured");
            int sleepHour = 0;
            int sleepMin = 0;
            int rebootHour = 0;
            int rebootMin = 0;
            Long sleepAlarmTime;
            Long rebootAlarmTime;

            try {
                sleepHour = mConfigMgr.mConfig.getInt("sleepHour");
                sleepMin = mConfigMgr.mConfig.getInt("sleepMin");
                rebootHour = mConfigMgr.mConfig.getInt("rebootHour");
                rebootMin = mConfigMgr.mConfig.getInt("rebootMin");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // SLEEP ------------------------------------------------
            if (sleepHour > 0) {
                Calendar now = Calendar.getInstance();
                Calendar sleepCalendar = Calendar.getInstance();
                sleepCalendar.setTimeInMillis(System.currentTimeMillis());
                sleepCalendar.set(Calendar.HOUR_OF_DAY, sleepHour);
                sleepCalendar.set(Calendar.MINUTE, sleepMin);

                if (sleepCalendar.getTimeInMillis() <= now.getTimeInMillis()) {
                    sleepAlarmTime = sleepCalendar.getTimeInMillis() + (AlarmManager.INTERVAL_DAY);
                } else {
                    sleepAlarmTime = sleepCalendar.getTimeInMillis();
                }

                Log.d(mLogTag, "sleep alarmTime: " + String.valueOf(sleepAlarmTime));
                mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, sleepAlarmTime, pendingSleepIntent);
                Log.d(mLogTag, "Sleep alarm set!: " + sleepHour + ":" + sleepMin);
            }

            // REBOOT ------------------------------------------------
            if (rebootHour > 0) {
                Calendar now = Calendar.getInstance();
                Calendar rebootCalendar = Calendar.getInstance();
                rebootCalendar.setTimeInMillis(System.currentTimeMillis());
                rebootCalendar.set(Calendar.HOUR_OF_DAY, rebootHour);
                rebootCalendar.set(Calendar.MINUTE, rebootMin);

                if (rebootCalendar.getTimeInMillis() <= now.getTimeInMillis()) {
                    rebootAlarmTime = rebootCalendar.getTimeInMillis() + (AlarmManager.INTERVAL_DAY);
                } else {
                    rebootAlarmTime = rebootCalendar.getTimeInMillis();
                }

                Log.d(mLogTag, "reboot alarmTime: " + String.valueOf(rebootAlarmTime));
                mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, rebootAlarmTime, pendingRebootIntent);
                Log.d(mLogTag, "Reboot alarm set!: " + rebootHour + ":" + rebootMin);
            }
        }
    }
}
