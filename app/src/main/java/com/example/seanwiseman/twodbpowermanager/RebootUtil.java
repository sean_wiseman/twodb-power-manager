package com.example.seanwiseman.twodbpowermanager;

import android.util.Log;

/**
 * Created by Sean on 13/05/2016.
 */

public class RebootUtil {

    String mLogTag = this.getClass().getSimpleName();

    public void reboot() {
        try {
            String[] reboot = new String[] { "su", "-c", "reboot", "now" };
            Runtime.getRuntime().exec(reboot);
        } catch (Exception ex) {
            Log.e(mLogTag, "Unable to reboot system");
            ex.printStackTrace();
        }
    }
}
