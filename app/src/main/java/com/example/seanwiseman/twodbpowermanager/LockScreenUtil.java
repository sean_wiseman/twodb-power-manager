package com.example.seanwiseman.twodbpowermanager;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Sean on 13/05/2016.
 */
public class LockScreenUtil {

    String mLogTag = this.getClass().getSimpleName();
    private Context mAppContext;
    private DevicePolicyManager mDevicePolicyMgr;
    private ComponentName mComponentName;

    public LockScreenUtil(Context appContext, DevicePolicyManager mgr, ComponentName cn) {
        mAppContext = appContext;
        mDevicePolicyMgr = mgr;
        mComponentName = cn;
    }

    public void lockMeNow() {
        if (mDevicePolicyMgr.isAdminActive(mComponentName)) {
            mDevicePolicyMgr.lockNow();
        }
        else {
            Intent intent= new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mComponentName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, mAppContext.getString(R.string.device_admin_explanation));
            mAppContext.startActivity(intent);
        }
    }
}
