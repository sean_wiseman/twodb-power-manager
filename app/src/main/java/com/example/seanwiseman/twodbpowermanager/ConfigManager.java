package com.example.seanwiseman.twodbpowermanager;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

/**
 * Created by Sean on 16/05/2016.
 */
public class ConfigManager {

    String mLogTag = this.getClass().getSimpleName();
    Context mAppContext;
    JSONObject mConfig;
    String mConfigStr;
    String mExternalConfigPath;
    boolean mHasBeenConfigured = false;
    static final int READ_BLOCK_SIZE = 100;

    public ConfigManager(Context appContext, String configFname) {
        mAppContext = appContext;
        mExternalConfigPath = configFname;
        mHasBeenConfigured = checkIfConfigured(mExternalConfigPath);

        if (mHasBeenConfigured) {
            // load from localstorage
            mConfigStr = loadConfigFromLocalStorage();
            Log.d(mLogTag, "Unit has been configured before");
            Log.d(mLogTag, mConfigStr);

            try {
                mConfig = new JSONObject(mConfigStr);
                Log.i(mLogTag, mConfig.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkIfConfigured(String fname) {
        File file = mAppContext.getFileStreamPath(fname);
        return file.exists();
    }

    public String loadConfigFromLocalStorage() {
        try {
            FileInputStream fileIn = mAppContext.openFileInput(mExternalConfigPath);
            InputStreamReader InputRead= new InputStreamReader(fileIn);

            char[] inputBuffer= new char[READ_BLOCK_SIZE];
            String jsonStr = "";
            int charRead;

            while ((charRead=InputRead.read(inputBuffer))>0) {
                // char to string conversion
                String readstr = String.copyValueOf(inputBuffer,0,charRead);
                jsonStr +=readstr;
            }
            InputRead.close();
            return jsonStr;

        } catch (Exception e) {
            e.printStackTrace();
            return "not found";
        }
    }

    public void saveConfigFile(String configStr) {
        try {
            Log.d("saveConfigFile", configStr);
            FileOutputStream fos = mAppContext.openFileOutput(mExternalConfigPath, mAppContext.MODE_PRIVATE);
            fos.write(configStr.getBytes());
            fos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void deleteLocalConfigFile() {
        try {
            File file = mAppContext.getFileStreamPath(mExternalConfigPath);
            file.delete();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
