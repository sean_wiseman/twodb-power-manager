package com.example.seanwiseman.twodbpowermanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    String mLogTag = this.getClass().getSimpleName();
    private Context mMainContext;
    private Button mSetSleepTimeButton;
    private Button mSetRebootTimeButton;
    private Button mResetTimesButton;
    private TimePicker mSleepTimepicker;
    private TimePicker mRebootTimepicker;
    private TextView mSetSleepTimeTextView;
    private TextView mSetRebootTimeTextView;

    private DevicePolicyManager mgr;
    private ComponentName cn;
    private ConfigManager mConfigMgr;

    private PendingIntent pendingSleepIntent;
    private PendingIntent pendingRebootIntent;
    private AlarmManager mAlarmManager;

    private int mSleepHour;
    private int mSleepMin;
    private int mRebootHour;
    private int mRebootMin;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cn = new ComponentName(this, AdminReceiver.class);
        mgr = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);

        mMainContext = getApplicationContext();
        //final LockScreenUtil mLockScreenMgr = new LockScreenUtil(mMainContext, mgr, cn);

        // Retrieve a PendingIntent that will perform a broadcast
        mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        // setup pending intents
        Intent mSleepAlarmIntent = new Intent(this, SleepAlarmReceiver.class);
        Intent mRebootAlarmIntent = new Intent(this, RebootAlarmReceiver.class);

        pendingSleepIntent = PendingIntent.getBroadcast(this, 0, mSleepAlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        pendingRebootIntent = PendingIntent.getBroadcast(this, 0, mRebootAlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        mSleepTimepicker = (TimePicker) findViewById(R.id.sleepTimePicker);
        mRebootTimepicker = (TimePicker) findViewById(R.id.rebootTimePicker);
        mSetSleepTimeButton = (Button) findViewById(R.id.setSleepTimeButton);
        mSetRebootTimeButton = (Button) findViewById(R.id.setRebootTimeButton);
        mResetTimesButton = (Button) findViewById(R.id.resetTimesButton);

        mSetSleepTimeTextView = (TextView) findViewById(R.id.setSleepTimeTextView);
        mSetRebootTimeTextView = (TextView) findViewById(R.id.setRebootTimeTextView);

        // CHECK FOR EXISTING CONFIG FILE
        mConfigMgr = new ConfigManager(mMainContext, "powerManagerConf.json");
        if (mConfigMgr.mHasBeenConfigured) {
            // SET THE SET TIME FIELDS IF CONFIG EXISTS
            String sleepHour = null;
            String sleepMin = null;
            String rebootHour = null;
            String rebootMin = null;
            try {
                sleepHour = mConfigMgr.mConfig.getString("sleepHour");
                sleepMin = mConfigMgr.mConfig.getString("sleepMin");
                rebootHour = mConfigMgr.mConfig.getString("rebootHour");
                rebootMin = mConfigMgr.mConfig.getString("rebootMin");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSetSleepTimeTextView.setText(sleepHour + ":" + zeroFormatStr(sleepMin));
            mSetRebootTimeTextView.setText(rebootHour + ":" + zeroFormatStr(rebootMin));

        }

        mSetSleepTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSleepHour = mSleepTimepicker.getCurrentHour();
                mSleepMin = mSleepTimepicker.getCurrentMinute();
                Long alarmTime;

                // Set the alarm to start at hour / minute
                Calendar now = Calendar.getInstance();
                Calendar sleepCalendar = Calendar.getInstance();
                //calendar.setTimeInMillis(System.currentTimeMillis());
                sleepCalendar.set(Calendar.HOUR_OF_DAY, mSleepHour);
                sleepCalendar.set(Calendar.MINUTE, mSleepMin);
                Long milli = sleepCalendar.getTimeInMillis();

                Log.d(mLogTag, "sleepCalendar : " + String.valueOf(milli) + ", now: " + String.valueOf(now.getTimeInMillis()));

                mAlarmManager.cancel(pendingSleepIntent);
                if (sleepCalendar.getTimeInMillis() <= now.getTimeInMillis()) {
                    alarmTime = sleepCalendar.getTimeInMillis() + (AlarmManager.INTERVAL_DAY);
                } else {
                    alarmTime = sleepCalendar.getTimeInMillis();
                }

                Log.d(mLogTag, "sleep alarmTime: " + String.valueOf(alarmTime));

                mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, pendingSleepIntent); // mAlarmManager.INTERVAL_DAY
                Toast.makeText(getApplicationContext(), "Sleep time set: " + mSleepHour + ":" + zeroFormat(mSleepMin), Toast.LENGTH_SHORT).show();
                mSetSleepTimeTextView.setText(mSleepHour + ":" + zeroFormat(mSleepMin));

                // WRITE TO CONF FILE
                saveCurrentConfig();

            }
        });

        mSetRebootTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRebootHour = mRebootTimepicker.getCurrentHour();
                mRebootMin = mRebootTimepicker.getCurrentMinute();
                Long alarmTime;

                // Set the alarm to start at hour / minute
                Calendar now = Calendar.getInstance();
                Calendar rebootCalendar = Calendar.getInstance();
                //calendar.setTimeInMillis(System.currentTimeMillis());
                rebootCalendar.set(Calendar.HOUR_OF_DAY, mRebootHour);
                rebootCalendar.set(Calendar.MINUTE, mRebootMin);

                mAlarmManager.cancel(pendingRebootIntent);

                Log.d(mLogTag, "rebootCalendar : " + String.valueOf(rebootCalendar.getTimeInMillis()) + ", now: " + String.valueOf(now.getTimeInMillis()));

                if (rebootCalendar.getTimeInMillis() <= now.getTimeInMillis()) {
                    alarmTime = rebootCalendar.getTimeInMillis() + (AlarmManager.INTERVAL_DAY);
                } else {
                    alarmTime = rebootCalendar.getTimeInMillis();
                }

                Log.d(mLogTag, "reboot alarmTime: " + String.valueOf(alarmTime));

                mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, pendingRebootIntent);
                Toast.makeText(getApplicationContext(), "Reboot time set: " + mRebootHour + ":" + zeroFormat(mRebootMin), Toast.LENGTH_SHORT).show();
                mSetRebootTimeTextView.setText(mRebootHour + ":" + zeroFormat(mRebootMin));

                // WRITE TO CONF FILE
                saveCurrentConfig();
            }
        });

        mResetTimesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mAlarmManager.cancel(pendingRebootIntent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                try {
                    mAlarmManager.cancel(pendingSleepIntent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                mConfigMgr.deleteLocalConfigFile();
                mSetRebootTimeTextView.setText(getResources().getString(R.string.blank_time));
                mSetSleepTimeTextView.setText(getResources().getString(R.string.blank_time));
                Toast.makeText(getApplicationContext(), "Times Reset", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void saveCurrentConfig() {
        JSONObject conf = new JSONObject();

        try {
            // NEED TO CHECK BOTH HAVE BEEN SET
            conf.put("sleepHour", mSleepHour);
            conf.put("sleepMin", mSleepMin);
            conf.put("rebootHour", mRebootHour);
            conf.put("rebootMin", mRebootMin);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mConfigMgr.saveConfigFile(conf.toString());
    }

    private String zeroFormat(int num){
        if (num <= 9) {
            return "0" + num;
        }
        return String.valueOf(num);
    }

    private String zeroFormatStr(String num){
        if (num.length() == 1) {
            return "0" + num;
        }
        return num;
    }

}
